import React from "react";
import { Heading, Slide, Text } from "spectacle";

export default ({ title, subtitle }) => (
  <Slide>
    <Heading fit caps>
      {props.title}
    </Heading>
    <Text>{props.subtitle}</Text>
  </Slide>
);
