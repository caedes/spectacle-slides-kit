![spectacle-slides-kit-logo](docs/spectacle-slides-kit.png)

# Spectacle slides kit

[Spectacle](https://github.com/FormidableLabs/spectacle) slides kit

## LICENSE

[MIT](./LICENSE.md)
